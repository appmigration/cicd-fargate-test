inputs = {
  aws_region      = "ca-central-1"
  aws_account_id  = "329193457145"
  project_name    = "appmigration-gitlab"
  owner           = "project-owner"
  env             = "dev"
  s3_state_bucket = "appmigration-gitlab-runner-poc"
}
