include "root" {
  path   = find_in_parent_folders()
  expose = true
}

terraform {
  source = "${get_parent_terragrunt_dir()}/..//terraform_modules/ecs-fargate-gitlab-runner-service"
}

locals {
  // Read environment specific's settings from env_inputs.hcl
  vars = read_terragrunt_config(find_in_parent_folders("env_inputs.hcl"))
}

inputs = merge(local.vars.inputs,
  {
    project_code = local.vars.inputs.project_name
    environment  = local.vars.inputs.env
    service_name = "appmigration-gitlab-runner-service"

    manager_instance_count     = 1
    manager_ecs_cluster_arn    = "arn:aws:ecs:${local.vars.inputs.aws_region}:${local.vars.inputs.aws_account_id}:cluster/appmigration-gitlab-dev-fargate-gitlab-runner"
    manager_docker_image       = "${local.vars.inputs.aws_account_id}.dkr.ecr.${local.vars.inputs.aws_region}.amazonaws.com/fargate-gitlab-runner-manager:latest"
    manager_subnet_ids         = ["subnet-0f56dfbe86d15beb7", "subnet-0d5a0b8e7263e597d"]
    manager_security_group_ids = ["sg-0ecc55b7eec7951b1"]
    gitlab_token_secret_arn    = "arn:aws:secretsmanager:${local.vars.inputs.aws_region}:${local.vars.inputs.aws_account_id}:secret:dev/appmigration/appmigration_gitlab_token-oYB66L"
    gitlab_url                = "https://gitlab.com/"
    gitlab_runner_concurrency = 10
    gitlab_runner_name_prefix = "gitlab_runner_appmigration"

    managers_configs = {
      dev_tool1 : {
        tags : ["dev", "tool1"]
        limit : 1 # Check the available IPs in worker subnet
        worker_docker_image : "${local.vars.inputs.aws_account_id}.dkr.ecr.${local.vars.inputs.aws_region}.amazonaws.com/fargate-gitlab-runner-worker:latest"
        worker_cpu : 256
        worker_memory : 512
        worker_ecs_cluster_arn : "arn:aws:ecs:${local.vars.inputs.aws_region}:${local.vars.inputs.aws_account_id}:cluster/appmigration-gitlab-dev-fargate-gitlab-runner-workers"
        worker_aws_region : local.vars.inputs.aws_region
        worker_subnet_id : "subnet-0f56dfbe86d15beb7"
        worker_security_group_id : "sg-03aa82b3a048f0871"
        worker_ssh_user : "user_1"
        worker_task_role_arn : "arn:aws:iam::${local.vars.inputs.aws_account_id}:role/ecs-task-role-appmigration"
      }
      dev_tool2_tool3 : {
        tags : ["dev", "tool2", "tool3"]
        limit : 1 # Check the available IPs in worker subnet
        worker_docker_image : "${local.vars.inputs.aws_account_id}.dkr.ecr.${local.vars.inputs.aws_region}.amazonaws.com/fargate-gitlab-runner-worker:latest"
        worker_cpu : 512
        worker_memory : 2048
        worker_ecs_cluster_arn : "arn:aws:ecs:${local.vars.inputs.aws_region}:${local.vars.inputs.aws_account_id}:cluster/appmigration-gitlab-dev-fargate-gitlab-runner-workers"
        worker_aws_region : local.vars.inputs.aws_region
        worker_subnet_id : "subnet-0f56dfbe86d15beb7"
        worker_security_group_id : "sg-03aa82b3a048f0871"
        worker_ssh_user : "user_2"
        worker_task_role_arn : "arn:aws:iam::${local.vars.inputs.aws_account_id}:role/ecs-task-role-appmigration"
      }
    }

    iam_permissions_boundary = "" # modify as required
  }
)

dependencies {
  paths = ["../ecs-cluster-for-managers", "../ecs-cluster-for-workers"]
}
